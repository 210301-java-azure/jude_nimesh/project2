### L4G-back-end

### Overview
The backend server of L4G utilizes the Spring Boot framework to handle multiple different requests from a client. <br>
The server's purpose is to return data from a SQL Server database when requested. <br>
Ther server has decent Test coverage, more tests to be written soon.  Please report any bugs found!

### Technologies used
- Spring MVC
- Spring Boot
- Spring Data
- Javalin
- Microsoft SQL Server
- Azure VM
- Jenkins
- Docker
- Junit

## Known Issues
- Steam API sometimes returns an unreadable format
- No functionality to delete a group
- If a user has comments/posts, an Admin may not be able to delete said user's account

### EndPoints

# AuthController
- Login Functionality: 
- passwords are hashed when they are stored
    - `POST /login` takes 2 params: `email` and `password` Allows a user to login
    - `POST /signup` takes 4 params:  `username` `email` `password` `userRole` Allows a user to signup

# UserController 
- User Functionality
    - `GET /users` gets all users
    - `GET /users/{id}` gets a user by their ID
    - `GET /users/groups/{id}` gets all Users in a specific group
    - `Get /users/{id}/groups` gets all groups that a user is in
    - `DELETE /users/{id}` deletes a user by their ID
    - `PUT /users/role{id}` updates the user role
    - `PUT /users/{id}` updates various user fields

# GroupController
- Group Functionality
    - `GET /groups` gets all groups
    - `GET /groups/{id}` gets a group by its ID
    - `POST /groups` 3 parameters: `name` of group, `userId` of user making the group, `description` of the group
        - method creates a group and automatically assigns moderator privileges to the user that made it
    - `DELETE /groups/{id}` deletes a group with given ID

# DiscussionController
- Discussion Functionality
    - `GET /discussions/{groupId}/group` gets all discussions in a given group
    - `GET /discussions/{userId}/user` gets all discussions a given user has made
    - `POST /discussions` with a JSON parameter, creates a new discussion in a group
    - `PUT /discussions` with a JSON parameter, edits a discussion in a group
    - `DELETE /discussions/{id}` deletes a discussions by a given ID

# CommentController
- Comments/Replies functionality
    - `GET /comments` gets all comments
    - `GET /comments/discussion/{id}` gets all comments for a given discussion ID
    - `GET /comments{id}/replies` gets all replies to a given comment
    - `GET /comments/user/{id}` gets all comments a user has posted
    - `POST /comments` takes a JSON parameter, creates a comment in a discussion
    - `DELETE /comments/{id}` deletes a given comennt by its ID

# UserGroupController
- UserGroup Join Table Functionality
- Mostly logic that the server handles to link the relationship between Users and Groups
    - `GET /user-group/user/{userId}/group/{groupId}` checks if a user is in a group
    - `POST /user-group` in JSON format, creates a new UserGroup link
    - `PUT /user-group` in JSON fomrat, used to update site Admin privilege
    - `DELETE /user-group/user/{id}/group/{id}` deletes a user from a given group (deletes the link for the user and group)

# ImageController
- Handles Image uploading logic
- Images are stored and processed as Byte Arrays
    - `GET /image` gets all images in database
    - `GET /image/group/{id}` gets an image for a group
    - `POST /image/upload` takes 3 parameters: `groupId` for with group it will be posted in, `userId` of the user that posts it, `imageFile` the file of the image
    - `DELETE /image/{groupId}` deletes the image from a given group

# SteamAPI Controller
- Handles calls to Steam's API
    - `GET /steam` takes one parameter, `appid` an Application ID for a steam game









